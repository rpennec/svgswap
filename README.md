SvgSwap
=======

[![svgswap-version](https://img.shields.io/badge/version-1.2-red.svg?style=flat)](https://gitlab.com/rpennec/svgswap/tags)

+ [Features](#how-does-it-work)
+ [Installation](#installation)
+ [License](#license)


Description
-----------

Author: Romain Pennec

Type: Application

Platform: Linux, Windows, OS X

License: GPL v3


What is that?
-------------

SvgSwap is a small application that helps you to change the colors of your eps, pdf or svg files.

It can be used in console mode or with the graphical user interface.

How does it work?
=================

![Drop Area](doc/PicDropArea.png)
![Converter](doc/PicConverter.png)

Once the application is installed (see instructions [here](#installation)), the procedure in GUI mode is the following

First scenario:
---------------

Let us say that you know which colors you want to replace. Here I mean that you know the name in hexadecimal or the rgb values or the cymk values of the colors present in your svg files and you also know the names (or rgb, or cymk) of the colors you want to replace them with.


1. Starts the application.

```
svgswap
```

2. Decide color replacement in the settings panel.

3. Drop svg files on the dedicated area (or click on it).


Second scenario:
----------------

Here we will work with one file only but I do not assume that you know the colors in your picture. You just find them ugly and want to change that, without opening Inkscape or like and having to change each object color.

1. Open the settings of the application and uncheck the automatic color replacement.
2. Drop your picture. The application will tell you which color are present and let you the possibility to change them.

![Second scenario](doc/Pic2ndScenario.png)
![Color selection](doc/PicColorSelection.png)

Console Mode
------------

Use this mode if you need to incorporate the features of this application in a script.

Usage example:

1. Write the configuration file for the color replacment:

```
[Colors]
%23......=#......
%23......=#......
```
where the first `......` in each line is the name in hexadecimal of the color to be replaced and the second if the name of the color that will replace it.

Save it, for example as `mycolor.conf`
2. Go in the directory with your pictures and run svgswap in console mode (`--console` or `-c`)

```
svgswap --console --setting-file=mycolor.conf --run-inkscape
```
If no argument is given it will replace the colors in each of the svg files in the current directory. Otherwise it will affect the svg file given in argument only. If you pass the option `--run-inkscape` then inkscape will be called after each modification like this:

```
inkscape -z -D -f <file.svg> --export-pdf=<file.pdf> --export-latex
```

If you do not use `setting-file` then the default configuration will be used, with the TUM colors.


You can query the colors in a svg file by running
```
svgswap --console --find-colors <file.svg>
```

Note that if some colors in the file are in format `rgb(..%,..%,..)` then they will be overwritten in the format `#......`. You can prevent this behavior by passing the option `--no-modif`.


Installation
============

You have two possibilities: either you download a ready-to-use binary version,
or you clone this project and compile the sources yourself. It depends on the
time you want to spend for this and on your paranoid relation with executable
found on the internet...

Download a release...
---------------------

Go on the [release page](https://gitlab.com/rpennec/svgswap/tags) and download
a binary file for your system. Currently only Windows 7(64bits) 
and Debian 8(64bits) are supported.

or Build the sources
--------------------

This application is written in C++ and requires Qt5.5 to be installed on your 
computer (it might work with an older version of Qt5 but I have not tested). 
It is your job to install it. Same for inkscape. 
See this [page](http://www.qt.io/download/) and this [page](https://inkscape.org/en/download/).

1. On Linux: run `qmake && make && sudo make install`
2. On Windows: compile and use it from QtCreator, or wait for me create an installation setup on day.
3. On OS X: probably same as linux.

You might need to install `podofocountpages`, `epstopdf` and `pdf2svg`.


License
=======

Copyright (C) 2015-2016 Romain Pennec

svgswap is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

svgswap is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with svgswap.  If not, see <http://www.gnu.org/licenses/>.
