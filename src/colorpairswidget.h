/****************************************************************************
**
**  Copyright (C) 2015-2016 Romain Pennec.
**  Contact: romain.pennec@tum.de
**
**  This file is a part of the svgswap project.
**  https://gitlab.com/rpennec/svgswap
**
**  $QT_BEGIN_LICENSE:GPL$
**
**  svgswap is free software: you can redistribute it and/or
**  modify it under the terms of the GNU General Public License as published
**  by the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  svgswap is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with svgswap.
**  If not, see <http://www.gnu.org/licenses/>.
**
** $QT_END_LICENSE$
**
****************************************************************************/

#ifndef COLORPAIRSWIDGET_H
#define COLORPAIRSWIDGET_H

#include <QWidget>
#include <QGridLayout>
#include "colorpairs.h"
#include "colorbutton.h"

#define MAX_NB_COLOR_IN_SVG 20

class ColorButton;

class ColorPairsWidget : public QWidget
{
    Q_OBJECT

public:
    explicit ColorPairsWidget(const ColorPairs &pairs, QWidget *parent=0);
    explicit ColorPairsWidget(const QList<QColor> &list, QWidget *parent=0);
    ColorPairs pairs() const;

public:
    void addToHistory(const ColorPairs &pairs);
    ColorPairs history() const;
    QColor replacementColorFor(const QColor &before) const;

signals:
    void pairChanged(const ColorPair &pair);

public slots:
    void updatePairs();
    void addLineInColorGrid();

protected:
    ColorPairs initializePairs(const QList<QColor> &list) const;

private:
    ColorPairs m_pairs;
    static ColorPairs s_history;
    QGridLayout *m_colorGrid;
    QList<ColorButton*> m_colorButtons;
};

#endif // COLORPAIRSWIDGET_H
