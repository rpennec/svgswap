/****************************************************************************
**
**  Copyright (C) 2015-2016 Romain Pennec.
**  Contact: romain.pennec@tum.de
**
**  This file is a part of the svgswap project.
**  https://gitlab.com/rpennec/svgswap
**
**  $QT_BEGIN_LICENSE:GPL$
**
**  svgswap is free software: you can redistribute it and/or
**  modify it under the terms of the GNU General Public License as published
**  by the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  svgswap is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with svgswap.
**  If not, see <http://www.gnu.org/licenses/>.
**
** $QT_END_LICENSE$
**
****************************************************************************/

#include "inkscapetools.h"
#include <QStandardPaths>
#include <QApplication>
#include <QStringList>
#include <QFileInfo>
#include <QProcess>
#include <QRegExp>
#include <QDebug>
#include <QColor>
#include <QFile>
#include <QTextStream>
#include <QDir>

#include "settings.h"

InkscapeTools::InkscapeTools(QObject *parent)
    : QObject(parent)
    , m_inkscape( new QProcess(this) )
    , m_pdflatex( new QProcess(this) )
{
    m_inkscape->setProgram("inkscape");
    m_pdflatex->setProgram("pdflatex");
    m_texPreamble = "\\usepackage{AMmath}\n"
                    "\\usepackage{lmodern}";
    this->checkSoftwares();
}

QStringList InkscapeTools::svgInDirectory(const QString &dirPath)
{
    QStringList list;
    QDir dir(dirPath);
    if( ! dir.exists() )
        return list;

    // search all the file with the extension svg
    QStringList searchExt = QStringList() << "*.svg";
    foreach(const QString &name, dir.entryList(searchExt,QDir::Files|QDir::Writable))
    {
        list << dir.absoluteFilePath(name);
    }

    return list;
}


QList<QColor> InkscapeTools::findColors(const QString &filePath, bool rcf)
{
    bool replaceColorFormat = rcf;
    QList<QColor> list;
    QRegExp re("#[0-9a-f]{6}");
    QRegExp rx("rgb\\(");

    // Container for the corrected files (if rcf is true)
    QStringList corrected;

    // First, open file
    QFile file(filePath);
    if( ! file.open(QFile::ReadOnly|QFile::Text) )
    {
        qWarning() << "Cannot open file";
        return list;
    }

    // Read file line by line
    QTextStream in(&file);
    while(!in.atEnd())
    {
        QString line = in.readLine();
        // search color by name (#xxxxx)
        int p = re.indexIn(line);
        if( p > 0 )
        {
            QString colorName = line.mid(p,7);
            if( ! list.contains(colorName) )
                list << QColor(colorName) ;
        }

        // search color by rgb syntax: rgb(x.xxxx;x.xxxx;x.xxxx)
        int q = rx.indexIn(line);
        if( q > 0 )
        {
            int end = line.indexOf(";",q);
            QString rgbName = line.mid(q,end-q);
            QColor color = colorFromSVGrgb(rgbName);
            if( color.isValid()  )
            {
                corrected << line.replace(line.mid(q,end-q),color.name());
                if( ! list.contains(color) )
                    list << color;
            }
            else
            {
                corrected << line;
            }
        }
        else
        {
            corrected << line;
        }

    }
    file.close();

    // Mofication of the file (rgb syntax is replaced by label)
    if ( replaceColorFormat )
    {
        if( file.open(QFile::WriteOnly|QFile::Text) )
        {
            qWarning() << "I change the color format in the svg file";
            QTextStream out(&file);
            foreach(const QString &line, corrected)
                out << line + "\n";
        }
        else
            qWarning() << "Cannot write file";
    }
    file.close();

    // Return the list of all the found colors
    return list;
}

QColor InkscapeTools::colorFromSVGrgb(const QString &rgbStr)
{
    QStringList rgb = rgbStr.split("%,");
    if( rgb.count() == 3 )
    {
        qreal r = rgb[0].remove("rgb(").toDouble() / 100.0;
        qreal g = rgb[1].toDouble() / 100.0;
        qreal b = rgb[2].remove("%)").toDouble() / 100.0;
        return QColor::fromRgbF(r,g,b);
    }
    return QColor();
}

void InkscapeTools::checkSoftwares()
{
    qDebug() << "Checking for softwares on the system...";

    m_inkscapeInstalled = ! QStandardPaths::findExecutable("inkscape").isEmpty();
    m_epstopdfInstalled = ! QStandardPaths::findExecutable("epstopdf").isEmpty();
    m_pdftosvgInstalled = ! QStandardPaths::findExecutable("pdf2svg").isEmpty();
    m_pdflatexInstalled = ! QStandardPaths::findExecutable("pdflatex").isEmpty();

    if( m_inkscapeInstalled )
        qDebug() << "found inkscape";
    else
        qWarning() << "inkscape not found";

    if( m_epstopdfInstalled )
        qDebug() << "found epstopdf";
    else
        qWarning() << "epstopdf not found";

    if( m_pdftosvgInstalled )
        qDebug() << "found pdf2svg";
    else
        qWarning() << "pdf2svg not found";

    if( m_pdflatexInstalled )
        qDebug() << "found pdflatex";
    else
        qWarning() << "pdflatex not found";

    emit inkscapeFound(m_inkscapeInstalled);
    emit pdflatexFound(m_pdftosvgInstalled);
}

QString InkscapeTools::texPreamble() const
{
    return m_texPreamble;
}

void InkscapeTools::setTexPreamble(const QString &texPreamble)
{
    m_texPreamble = texPreamble;
}

void InkscapeTools::runInkscape(const QString &filePath, Export mode)
{
    // get file base name
    QFileInfo info(filePath);
    m_inkscape->setWorkingDirectory(info.absolutePath());
    QString fileName = info.baseName();

    // arguments for inkscape
    QStringList args;
    args << "-D"
         << "-z"
         << "--file="+fileName+"."+info.suffix();
    switch( mode )
    {
    case Latex:
        args << "--export-pdf="+fileName+".pdf"
             << "--export-latex";
        break;
    case Svg:
        args << "--export-plain-svg="+fileName+".svg";
        break;
    case Pdf:
        args << "--export-pdf="+fileName+".pdf";
        break;
    case Png:
        args << "--export-png="+fileName+".png";
        break;
    case Eps:
        args << "--export-eps"+fileName+".eps";
        break;
    }
    m_inkscape->setArguments(args);

    qDebug() << "Running inkscape:" << args;

    // start inkscape and wait for it to finish
    m_inkscape->start();
    bool started = m_inkscape->waitForStarted(1000);
    bool finished = m_inkscape->waitForFinished(3000);
    m_errorString = m_inkscape->errorString();

    emit inskapeFinished(started && finished
                         && m_inkscape->exitStatus()==QProcess::NormalExit);
}

void InkscapeTools::runPdfLatex(const QString &filePath)
{
    // get file base name
    QFileInfo info(filePath);
    m_pdflatex->setWorkingDirectory(info.absolutePath());
    QString fileName = info.baseName();

    // arguments for pdflatex
    QStringList args;
    args << "-jobname=\""+fileName+"-svgswap\""
         << fileName+"."+info.suffix();

    m_pdflatex->setArguments(args);

    qDebug() << "Running pdflatex:" << args;

    // start inkscape and wait for it to finish
    m_pdflatex->start();
    bool started = m_pdflatex->waitForStarted(2000);
    bool finished = m_pdflatex->waitForFinished(10000);
    m_errorString = m_pdflatex->errorString();

    emit pdflatexFinished(started && finished
                          && m_pdflatex->exitStatus()==QProcess::NormalExit);
}

bool InkscapeTools::generateTexFileFor(const QString &pdf_texFile)
{
    QFileInfo info(pdf_texFile);

    QStringList lines;
    lines << "\\documentclass[border=1mm]{standalone}";
    lines << m_texPreamble;
    lines << "\\begin{document}";
    lines << "\\input{" + info.baseName() + ".pdf_tex}";
    lines << "\\end{document}";
    lines << "";

    QString texFilePath = QString(pdf_texFile).replace(".pdf_tex",".tex");
    QFile texFile(texFilePath);
    if ( texFile.open(QFile::WriteOnly|QFile::Text) )
    {
        QTextStream out(&texFile);
        out << lines.join("\n");
        texFile.close();
        qDebug() << "Generated file" << texFilePath;
        return true;
    }
    else
    {
        qWarning() << "Cannot write in" << texFilePath;
        return false;
    }
}

void InkscapeTools::replaceColors(const QString &filePath)
{
    this->replaceColors(filePath,s->replacementPairs());
}

void InkscapeTools::replaceColors(const QString &filePath, const ColorPairs &pairs)
{
    // First open the file
    QFile file(filePath);
    if( ! file.open(QFile::ReadWrite|QFile::Text) )
    {
        qWarning() << "Cannot open file";
        return ;
    }

    // Read the file line by line. It content is written in the variable text.
    // Each time a line contains a color name, it is replaced.
    QTextStream in(&file);
    QStringList text;
    while(!in.atEnd())
    {
        QString line = in.readLine();

        foreach(const ColorPair &pair, pairs)
        {
            line.replace(pair.first,pair.second);
        }
        text << line;
    }
    file.close();

    // Overwrite the file by the variable text.
    if( file.open(QFile::WriteOnly|QFile::Text) )
    {
        QTextStream out(&file);
        out << text.join("\n");
    }
    else
    {
        qWarning() << tr("Cannot write file");
        return ;
    }

    // Run inkscape again.
    if( s->runInkscapeAfterReplacement() )
    {
        this->runInkscape(filePath);
    }

    emit replacedColors(file.fileName());
}

void InkscapeTools::replaceColors(const QStringList &filePaths)
{
    foreach(const QString &path, filePaths)
    {
        this->replaceColors(path);
        qDebug() << "Replaced colors in" << QFileInfo(path).baseName();
    }
}


