/****************************************************************************
**
**  Copyright (C) 2015-2016 Romain Pennec.
**  Contact: romain.pennec@tum.de
**
**  This file is a part of the svgswap project.
**  https://gitlab.com/rpennec/svgswap
**
**  $QT_BEGIN_LICENSE:GPL$
**
**  svgswap is free software: you can redistribute it and/or
**  modify it under the terms of the GNU General Public License as published
**  by the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  svgswap is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with svgswap.
**  If not, see <http://www.gnu.org/licenses/>.
**
** $QT_END_LICENSE$
**
****************************************************************************/

#include "converterwidget.h"

#include <QDesktopServices>
#include <QDialogButtonBox>
#include <QPushButton>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QMessageBox>
#include <QTextEdit>
#include <QFileInfo>
#include <QToolBar>
#include <QAction>
#include <QDialog>
#include <QLabel>
#include <QIcon>
#include <QUrl>
#include <QDir>

#include <QDebug>
#include "dropfileswidget.h"
#include "inkscapetools.h"
#include "settings.h"

ConverterWidget::ConverterWidget(QWidget *parent)
    : QWidget(parent)
    , m_converter( NULL )
    , m_convertBar( new QToolBar(this) )
{
    m_actConvert2Pdf = new QAction(QIcon(":/icon/pdf-filetype"),"to PDF",this);
    m_actConvert2Svg = new QAction(QIcon(":/icon/svg-filetype"),"to SVG",this);
    m_actConvert2Png = new QAction(QIcon(":/icon/png-filetype"),"to PNG",this);
    m_actConvert2Eps = new QAction(QIcon(":/icon/eps-filetype"),"to EPS",this);
    QAction *actBack = new QAction(QIcon::fromTheme("go-previous",QIcon(":/icon/go-previous")),"back",this);

    DropFilesWidget *dropWidget = new DropFilesWidget(this);
    dropWidget->setText("Drop file\n here\n to convert it");
    dropWidget->setMultipleDropEnabled(false);

    m_currentLabel = new QLabel(this);
    m_currentLabel->setText(tr("none"));
    m_currentIcon = new QPushButton(this);
    m_currentIcon->setFixedSize(35,35);
    m_currentIcon->setFlat(true);
    m_currentIcon->setIconSize(QSize(30,30));

    m_convertBar->setMovable(false);
    m_convertBar->setOrientation(Qt::Vertical);
    m_convertBar->setToolButtonStyle(Qt::ToolButtonTextBesideIcon);
    m_convertBar->addAction(m_actConvert2Pdf);
    m_convertBar->addAction(m_actConvert2Svg);
    m_convertBar->addAction(m_actConvert2Png);
    m_convertBar->addAction(m_actConvert2Eps);

    QToolBar *backBar = new QToolBar(this);
    backBar->setMovable(false);
    backBar->setToolButtonStyle(Qt::ToolButtonTextBesideIcon);
    backBar->setOrientation(Qt::Horizontal);
    backBar->addAction(actBack);

    QHBoxLayout *hlayout = new QHBoxLayout;
    hlayout->addWidget(dropWidget);
    hlayout->addWidget(m_convertBar);

    QHBoxLayout *currLayout = new QHBoxLayout;
    currLayout->addWidget(new QLabel(tr("Current file:"),this));
    currLayout->addWidget(m_currentIcon);
    currLayout->addWidget(m_currentLabel,1);

    QVBoxLayout *vlayout = new QVBoxLayout;
    vlayout->addLayout(currLayout);
    vlayout->addLayout(hlayout);
    vlayout->addWidget(backBar);

    this->setLayout(vlayout);

    connect(actBack,&QAction::triggered,this,&ConverterWidget::done);
    connect(dropWidget,&DropFilesWidget::fileDropped,[=](const QStringList paths){
        if( ! paths.isEmpty() )
            this->setCurrentFile(paths.first());

        this->updateConvertButtons();
    });
    connect(m_currentIcon,&QPushButton::clicked,[=](bool checked){
        Q_UNUSED(checked);
        if( m_currentExt == "pdf_tex" )
            this->openPreambleEditor();
        else
        {
            QUrl url(m_currentFile);
            if( url.isValid() )
                QDesktopServices::openUrl(url);
        }
    });

    this->updateConvertButtons();
}

void ConverterWidget::setConverter(InkscapeTools *converter)
{
    if ( m_converter )
        qCritical() << "Converter already set.";

    m_converter = converter;
    if( m_converter )
    {
        connect(m_actConvert2Pdf,&QAction::triggered,[=](bool checked){
            Q_UNUSED(checked);
            if( m_currentExt == "pdf_tex" )
                this->convertTo(".pdf_tex");
            else
                this->convertTo(".pdf");
        });
        connect(m_actConvert2Svg,&QAction::triggered,[=](bool checked){
            Q_UNUSED(checked);
            this->convertTo(".svg");
        });
        connect(m_actConvert2Png,&QAction::triggered,[=](bool checked){
            Q_UNUSED(checked);
            this->convertTo(".png");
        });
        connect(m_actConvert2Eps,&QAction::triggered,[=](bool checked){
            Q_UNUSED(checked);
            this->convertTo(".eps");
        });
    }
}

void ConverterWidget::setCurrentFile(const QString &filePath)
{
    m_currentFile = filePath;
    QFileInfo fileInfo(filePath);
    m_currentExt = fileInfo.suffix().toLower();
    m_currentLabel->setText(fileInfo.baseName());
    qDebug() << m_currentFile << m_currentExt;
}

void ConverterWidget::updateConvertButtons()
{
    if( m_converter )
    {
        foreach(QAction *action, m_convertBar->actions())
            action->setEnabled(true);
    }

    m_currentIcon->setFlat(true);

    if( m_currentExt == "pdf" )
    {
        m_actConvert2Pdf->setEnabled(false);
        m_currentIcon->setIcon(QIcon(":/icon/pdf-filetype.png"));
    }
    else if( m_currentExt == "svg" )
    {
        m_actConvert2Svg->setEnabled(false);
        m_currentIcon->setIcon(QIcon(":/icon/svg-filetype.png"));
    }
    else if( m_currentExt == "png" )
    {
        m_actConvert2Png->setEnabled(false);
        m_currentIcon->setIcon(QIcon(":/icon/png-filetype.png"));
    }
    else if( m_currentExt == "eps" )
    {
        m_actConvert2Eps->setEnabled(false);
        m_currentIcon->setIcon(QIcon(":/icon/eps-filetype.png"));
    }
    else if( m_currentExt == "pdf_tex" )
    {
        m_actConvert2Eps->setEnabled(false);
        m_actConvert2Png->setEnabled(false);
        m_actConvert2Svg->setEnabled(false);
        m_currentIcon->setIcon(QIcon(":/icon/tex-filetype.png"));
    }
    else
    {
        m_currentIcon->setIcon(QIcon());
        foreach(QAction *action, m_convertBar->actions())
            action->setEnabled(false);
    }
}

void ConverterWidget::convertTo(const QString &ext)
{
    InkscapeTools::Export mode;
    if( ext == ".pdf" )
        mode = InkscapeTools::Pdf;
    else if( ext == ".png" )
        mode = InkscapeTools::Png;
    else if( ext == ".svg" )
        mode = InkscapeTools::Svg;
    else if( ext == ".pdf_tex" )
    {
        QString texPath = QString(m_currentFile).replace(".pdf_tex",".tex");
        if( m_converter->generateTexFileFor(texPath) )
            m_converter->runPdfLatex(texPath);

        return ;
    }
    else
    {
        qWarning() << "Unknown conversion extension" << ext;
        return ;
    }

    QFileInfo info(m_currentFile);
    QDir outputDir = info.absoluteDir();
    bool alreadyExists = outputDir.exists(info.baseName()+ext);

    if( ! alreadyExists
        || s->overwrite()
        || QMessageBox::question(this,tr("File already exists")
             , tr("File %1 already exists!\n"
                  "Do you want to overwrite it?").arg(info.baseName()+ext)
             , QMessageBox::Yes|QMessageBox::No )
           == QMessageBox::Yes )
    {
        m_converter->runInkscape(m_currentFile,mode);
    }

}

void ConverterWidget::openPreambleEditor()
{
    QDialog *dialog = new QDialog(this);
    QDialogButtonBox *buttonBox = new QDialogButtonBox(QDialogButtonBox::Ok
                                                       | QDialogButtonBox::Cancel);
    QTextEdit *preambleEditor = new QTextEdit(dialog);
    preambleEditor->setText(m_converter->texPreamble());

    connect(buttonBox,&QDialogButtonBox::accepted,[=](){
        m_converter->setTexPreamble(preambleEditor->toPlainText());
        dialog->accept();
    });
    connect(buttonBox,&QDialogButtonBox::rejected,dialog,&QDialog::reject);

    QVBoxLayout *layout = new QVBoxLayout;
    layout->addWidget(preambleEditor);
    layout->addWidget(buttonBox);
    dialog->setLayout(layout);
    dialog->exec();

}

