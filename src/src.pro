
CONFIG += c++11
QT     += core gui widgets svg

TARGET = svgswap 
TEMPLATE = app
CONFIG += warn_on

DESTDIR = ../bin
MOC_DIR = ../build/moc
RCC_DIR = ../build/rcc

unix:OBJECTS_DIR = ../build/o/unix
win32:OBJECTS_DIR = ../build/o/win32
macx:OBJECTS_DIR = ../build/o/mac

SOURCES +=\
    colorbutton.cpp \
    colorpairswidget.cpp \
    dropfileswidget.cpp \
    graphicswidget.cpp \
    inkscapetools.cpp \
    main.cpp \
    mainwindow.cpp \
    settingswidget.cpp \
    settings.cpp \
    colorpairs.cpp \
    converterwidget.cpp \
    inkscapelogwidget.cpp

HEADERS  += \
    colorbutton.h \
    colorpairswidget.h \
    dropfileswidget.h \
    graphicswidget.h \
    inkscapetools.h \
    mainwindow.h \
    settingswidget.h \
    settings.h \
    colorpairs.h \
    converterwidget.h \
    inkscapelogwidget.h

RESOURCES += \
    resources.qrc
