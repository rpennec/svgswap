/****************************************************************************
**
**  Copyright (C) 2015-2016 Romain Pennec.
**  Contact: romain.pennec@tum.de
**
**  This file is a part of the svgswap project.
**  https://gitlab.com/rpennec/svgswap
**
**  $QT_BEGIN_LICENSE:GPL$
**
**  svgswap is free software: you can redistribute it and/or
**  modify it under the terms of the GNU General Public License as published
**  by the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  svgswap is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with svgswap.
**  If not, see <http://www.gnu.org/licenses/>.
**
** $QT_END_LICENSE$
**
****************************************************************************/

#include "dropfileswidget.h"

#include <QVBoxLayout>
#include <QDropEvent>
#include <QApplication>
#include <QCursor>
#include <QDragEnterEvent>
#include <QFileDialog>
#include <QDebug>
#include <QMimeData>
#include <QFrame>
#include <QLabel>

DropFilesWidget::DropFilesWidget(QWidget *parent)
    : QWidget( parent )
    , m_frame ( new QFrame(this) )
    , m_label ( new QLabel(this) )
{
    m_multipleDropEnabled = true;

    m_label->setAlignment(Qt::AlignCenter);
    m_label->setText(tr("Drop files here"));

    m_frame->setStyleSheet("QFrame {background-color: #ffffff; border: 2px solid #0065bd; border-radius: 4px; padding: 2px; }");
    m_label->setStyleSheet("border: 0px; font: 18pt;");
    m_label->installEventFilter(this);

    // Layout for the QFrame
    QVBoxLayout *frameLayout = new QVBoxLayout;
    frameLayout->addStretch();
    frameLayout->addWidget(m_label);
    frameLayout->addStretch();
    m_frame->setLayout(frameLayout);

    // Main layout (for the margins)
    QVBoxLayout *layout = new QVBoxLayout;
    layout->setMargin(25);
    layout->addWidget(m_frame);
    this->setLayout(layout);

    // Drop settings
    this->setAcceptDrops(true);
    connect(this,&DropFilesWidget::textClicked, &DropFilesWidget::dialogSelectPaths);
}

void DropFilesWidget::setFilter(const QString &filter)
{
    m_filter = filter;
    m_extensions.clear();

    QString strExt = filter;
    int ind1 = filter.indexOf("(");
    int ind2 = filter.indexOf(")");
    if ( ind1 > 0 && ind2 > ind1 )
    {
        strExt.remove(0,ind1+1);
        strExt.truncate(ind2-ind1-1);
    }

    foreach(const QString &ext, strExt.split(" "))
        m_extensions << QString(ext).remove("*");
}

QString DropFilesWidget::filter() const
{
    return m_filter;
}

void DropFilesWidget::setText(const QString &text)
{
    m_label->setText(text);
}

void DropFilesWidget::setMultipleDropEnabled(bool enabled)
{
    m_multipleDropEnabled = enabled;
}

void DropFilesWidget::dialogSelectPaths()
{
    QStringList paths;
    if ( m_multipleDropEnabled )
         paths = QFileDialog::getOpenFileNames(this,QString(),QString(),m_filter);
    else
        paths << QFileDialog::getOpenFileName(this,QString(),QString(),m_filter);

    emit fileDropped(paths);
}

void DropFilesWidget::dragEnterEvent(QDragEnterEvent *event)
{
    if (event->mimeData()->hasUrls())
    {
        if( m_multipleDropEnabled || event->mimeData()->urls().count() == 1 )
            event->acceptProposedAction();
    }
}

void DropFilesWidget::dropEvent(QDropEvent *event)
{
    QStringList pathList;

    foreach(const QUrl &url, event->mimeData()->urls())
    {
        QString strUrl = url.toLocalFile();
        if( m_extensions.isEmpty() || m_extensions.contains(strUrl.right(4)) )
            pathList << strUrl;
    }

    emit fileDropped(pathList);
    event->acceptProposedAction();
}

bool DropFilesWidget::eventFilter(QObject *obj, QEvent *event)
{
    if( obj == m_label )
    {
        if( event->type() == QMouseEvent::Enter )
        {
            QApplication::setOverrideCursor(QCursor(Qt::PointingHandCursor));
        }
        if( event->type() == QMouseEvent::Leave )
        {
            QApplication::setOverrideCursor(QCursor(Qt::ArrowCursor));
        }
        if( event->type() == QMouseEvent::MouseButtonRelease )
        {
            emit textClicked();
        }
    }
    return QWidget::eventFilter(obj,event);
}
