/****************************************************************************
**
**  Copyright (C) 2015-2016 Romain Pennec.
**  Contact: romain.pennec@tum.de
**
**  This file is a part of the svgswap project.
**  https://gitlab.com/rpennec/svgswap
**
**  $QT_BEGIN_LICENSE:GPL$
**
**  svgswap is free software: you can redistribute it and/or
**  modify it under the terms of the GNU General Public License as published
**  by the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  svgswap is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with svgswap.
**  If not, see <http://www.gnu.org/licenses/>.
**
** $QT_END_LICENSE$
**
****************************************************************************/

#include "settingswidget.h"

#include <QVBoxLayout>
#include <QGridLayout>
#include <QLabel>
#include <QPushButton>
#include <QDialogButtonBox>
#include <QCheckBox>
#include <QDebug>

#include "colorbutton.h"
#include "colorpairswidget.h"
#include "settings.h"

SettingsWidget::SettingsWidget(QWidget *parent)
    : QWidget(parent)
{   
    // bool settings
    QCheckBox *runInkscapeCheckBox = new QCheckBox(tr("Run inkscape after color replacement"),this);
    QCheckBox *manualCheckBox = new QCheckBox(tr("Manual color replacement"),this);
    QCheckBox *rememberCheckBox = new QCheckBox(tr("Remember replacement choices"),this);
    QCheckBox *backupsCheckBox = new QCheckBox(tr("Create backups"),this);
    QCheckBox *overwriteCheckBox = new QCheckBox(tr("Overwrite existing files when converting"));

    // create button bar ( ok )
    QDialogButtonBox *buttons = new QDialogButtonBox(this);
    QPushButton *okButton = buttons->addButton(QDialogButtonBox::Ok);
    connect(okButton,&QPushButton::clicked,this,&SettingsWidget::okClicked);

    // colors settings for automatic replacement
    QPushButton *newColorButton = buttons->addButton(tr("Add color to be replaced"),QDialogButtonBox::ActionRole);
    newColorButton->setIcon(QIcon::fromTheme("list-add"));
    ColorPairsWidget *pairWidget = new ColorPairsWidget(s->replacementPairs(),this);
    connect(newColorButton,&QPushButton::clicked,pairWidget,&ColorPairsWidget::addLineInColorGrid);
    connect(pairWidget,&ColorPairsWidget::pairChanged,s,&Settings::setReplaceColor);

    // connect setters
    connect(runInkscapeCheckBox, &QCheckBox::toggled,
            s, &Settings::setRunInkscapeAfterReplacement);
    connect(manualCheckBox, &QCheckBox::toggled,
            s, &Settings::setManualReplacement);
    connect(rememberCheckBox, &QCheckBox::toggled,
            s, &Settings::setRememberChoices);
    connect(manualCheckBox,&QCheckBox::toggled, [=](bool checked) {
        pairWidget->setVisible(!checked);
        newColorButton->setVisible(!checked);
    } );
    connect(backupsCheckBox, &QCheckBox::toggled,
            s, &Settings::setCreateBackups);
    backupsCheckBox->setEnabled(false);
    connect(overwriteCheckBox, &QCheckBox::toggled,
            s, &Settings::setOverwrite);

    // initialize settings
    runInkscapeCheckBox->setChecked(s->runInkscapeAfterReplacement());
    manualCheckBox->setChecked(s->manualReplacement());
    rememberCheckBox->setChecked(s->rememberChoices());
    backupsCheckBox->setChecked(s->createBackups());
    overwriteCheckBox->setChecked(s->overwrite());

    // assemble widget
    QVBoxLayout *layout = new QVBoxLayout;
    layout->addWidget(runInkscapeCheckBox,0,Qt::AlignTop);
    layout->addWidget(manualCheckBox,0,Qt::AlignTop);
    layout->addWidget(rememberCheckBox,0,Qt::AlignTop);
    layout->addWidget(backupsCheckBox,0,Qt::AlignTop);
    layout->addWidget(overwriteCheckBox,0,Qt::AlignTop);
    layout->addWidget(pairWidget);
    layout->addSpacing(10);
    layout->addWidget(buttons,1,Qt::AlignBottom);
    this->setLayout(layout);
}


