/****************************************************************************
**
**  Copyright (C) 2015-2016 Romain Pennec.
**  Contact: romain.pennec@tum.de
**
**  This file is a part of the svgswap project.
**  https://gitlab.com/rpennec/svgswap
**
**  $QT_BEGIN_LICENSE:GPL$
**
**  svgswap is free software: you can redistribute it and/or
**  modify it under the terms of the GNU General Public License as published
**  by the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  svgswap is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with svgswap.
**  If not, see <http://www.gnu.org/licenses/>.
**
** $QT_END_LICENSE$
**
****************************************************************************/

#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QStackedWidget>
#include <QBasicTimer>

QT_FORWARD_DECLARE_CLASS(QDockWidget)
class InkscapeTools;
class DropFilesWidget;
class SettingsWidget;
class ConverterWidget;

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = 0);
    ~MainWindow();

signals:
    void svgSelected(const QString &path);

public slots:
    void about();
    void showSettings();
    void showDropArea();
    void showConverter();

protected slots:
    void onDroppedFiles(const QStringList &list);
    void manualColorReplacment(const QString &path);

protected:
    void timerEvent(QTimerEvent *event);

private:
    QBasicTimer m_timer;
    InkscapeTools *m_tools;
    QStackedWidget *m_stack;
    SettingsWidget *m_settingsWidget;
    DropFilesWidget *m_dropWidget;
    ConverterWidget *m_converterWidget;
    QDockWidget *m_inkscapeDockWidget;
};


#endif // MAINWINDOW_H
