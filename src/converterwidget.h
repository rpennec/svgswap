/****************************************************************************
**
**  Copyright (C) 2015-2016 Romain Pennec.
**  Contact: romain.pennec@tum.de
**
**  This file is a part of the svgswap project.
**  https://gitlab.com/rpennec/svgswap
**
**  $QT_BEGIN_LICENSE:GPL$
**
**  svgswap is free software: you can redistribute it and/or
**  modify it under the terms of the GNU General Public License as published
**  by the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  svgswap is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with svgswap.
**  If not, see <http://www.gnu.org/licenses/>.
**
** $QT_END_LICENSE$
**
****************************************************************************/

#ifndef CONVERTERWIDGET_H
#define CONVERTERWIDGET_H

#include <QWidget>
QT_FORWARD_DECLARE_CLASS(QLabel)
QT_FORWARD_DECLARE_CLASS(QPushButton)
QT_FORWARD_DECLARE_CLASS(QToolBar)
QT_FORWARD_DECLARE_CLASS(QAction)

class InkscapeTools;

class ConverterWidget : public QWidget
{
    Q_OBJECT

public:
    explicit ConverterWidget(QWidget *parent = 0);
    void setConverter(InkscapeTools *converter);

signals:
    void done();

public slots:
    void setCurrentFile(const QString &filePath);
    void updateConvertButtons();
    void convertTo(const QString &ext);
    void openPreambleEditor();

private:
    InkscapeTools *m_converter;
    QToolBar *m_convertBar;
    QLabel *m_currentLabel;
    QPushButton *m_currentIcon;
    QString m_currentFile;
    QString m_currentExt;
    QAction *m_actConvert2Pdf;
    QAction *m_actConvert2Png;
    QAction *m_actConvert2Svg;
    QAction *m_actConvert2Eps;
};

#endif // CONVERTERWIDGET_H
