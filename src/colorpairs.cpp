/****************************************************************************
**
**  Copyright (C) 2015-2016 Romain Pennec.
**  Contact: romain.pennec@tum.de
**
**  This file is a part of the svgswap project.
**  https://gitlab.com/rpennec/svgswap
**
**  $QT_BEGIN_LICENSE:GPL$
**
**  svgswap is free software: you can redistribute it and/or
**  modify it under the terms of the GNU General Public License as published
**  by the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  svgswap is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with svgswap.
**  If not, see <http://www.gnu.org/licenses/>.
**
** $QT_END_LICENSE$
**
****************************************************************************/

#include "colorpairs.h"

ColorPair::ColorPair(const QString &before, const QString &after)
    : QPair<QString,QString>(before,after)
{

}

ColorPair::ColorPair(const QColor &before, const QColor &after)
{
    this->first = before.name();
    this->second = after.name();
}

QColor ColorPair::before() const
{
    return QColor(this->first);
}

QColor ColorPair::after() const
{
    return QColor(this->second);
}
