/****************************************************************************
**
**  Copyright (C) 2015-2016 Romain Pennec.
**  Contact: romain.pennec@tum.de
**
**  This file is a part of the svgswap project.
**  https://gitlab.com/rpennec/svgswap
**
**  $QT_BEGIN_LICENSE:GPL$
**
**  svgswap is free software: you can redistribute it and/or
**  modify it under the terms of the GNU General Public License as published
**  by the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  svgswap is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with svgswap.
**  If not, see <http://www.gnu.org/licenses/>.
**
** $QT_END_LICENSE$
**
****************************************************************************/

#include "colorbutton.h"

#include <QString>
#include <QPalette>
#include <QColorDialog>

ColorButton::ColorButton(const QColor &color, QWidget *parent)
    : QPushButton(parent)
    , m_id( generateID() )
    , m_original(color)
    , m_color(color)
{
    this->construct();
}

ColorButton::ColorButton(const QString &colorName, QWidget *parent)
    : QPushButton(parent)
    , m_id( generateID() )
    , m_original(QColor(colorName))
    , m_color(QColor(colorName))
{
    this->construct();
}

int ColorButton::id() const
{
    return m_id;
}

QColor ColorButton::color() const
{
    return m_color;
}

void ColorButton::selectNewColor()
{
    QColor before = m_color;
    m_color = QColorDialog::getColor(before,this,QString(),QColorDialog::DontUseNativeDialog);
    if( m_color != before )
    {
        emit colorChanged(m_color);
    }
}

void ColorButton::construct()
{
    this->setToolTip(m_color.name());
    this->updateColor();

    connect(this, &QPushButton::clicked, this, &ColorButton::selectNewColor);
    connect(this, &ColorButton::colorChanged, &ColorButton::updateColor);
}

int ColorButton::generateID() const
{
    static int s_itemID = 0;
    return s_itemID++;
}

void ColorButton::updateColor()
{
    this->setStyleSheet(QString("ColorButton {"
                        "background-color: %1;"
                        "border-style: outset;"
                        "border-width: 2px;"
                        "border-radius: 10px;"
                        "border-color: beige;"
                        "min-width: 1em;"
                        "padding: 6px;"
                        "} "
                        "ColorButton:hover:pressed {"
                        "border-style: inset;"
                        "border-color: black;} "
                        "ColorButton:hover {"
                        "border-style: inset;"
                        "border-color: gray;}").arg(m_color.name()));
}
