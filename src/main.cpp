/****************************************************************************
**
**  Copyright (C) 2015-2016 Romain Pennec.
**  Contact: romain.pennec@tum.de
**
**  This file is a part of the svgswap project.
**  https://gitlab.com/rpennec/svgswap
**
**  $QT_BEGIN_LICENSE:GPL$
**
**  svgswap is free software: you can redistribute it and/or
**  modify it under the terms of the GNU General Public License as published
**  by the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  svgswap is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with svgswap.
**  If not, see <http://www.gnu.org/licenses/>.
**
** $QT_END_LICENSE$
**
****************************************************************************/

#include "mainwindow.h"
#include <QApplication>
#include <QStringList>
#include <QCommandLineParser>
#include <QCommandLineOption>
#include <QStandardPaths>
#include <QFileInfo>
#include <QObject>
#include <QDebug>
#include <QDir>

#include "inkscapetools.h"
#include "settings.h"

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    a.setOrganizationName("gnec");
    a.setApplicationName("svgswap");
    a.setApplicationVersion("1.2");
    a.setWindowIcon(QIcon(":/logo/svgswap.png"));

    /// Block Command line parser
    QCommandLineParser parser;
    parser.setApplicationDescription("svgswap helper");
    parser.addHelpOption();
    parser.addVersionOption();

    // Source directory of the svg files
    parser.addPositionalArgument("source", QObject::tr("Directory of the svg files"));

    // Declare console option (-c, --console)
    QCommandLineOption consoleOption(QStringList() << "c" << "console", QObject::tr("Console mode. Gui will not be started."));
    parser.addOption(consoleOption);

    // Declare settings option ( --setting-file= )
    QCommandLineOption settingFileOption(
                QStringList() << "s" << "setting-file",
                QObject::tr("Use the setting file <file> for the colors."),
                QObject::tr("file"));
    parser.addOption(settingFileOption);

    // Declare find-colors option ( --find-colors )
    QCommandLineOption findColorsOption(QStringList() << "f" << "find-colors",QObject::tr("Use this options to list colors in a svg."));
    parser.addOption(findColorsOption);

    // Process the command line arguments given by the user
    parser.process(a);
    /// End of Block Command line parser

    // GUI MODE
    if( ! parser.isSet(consoleOption) )
    {
        qDebug() << "Entering GUI mode. (hint: pass option console for the CONSOLE mode.)";
        MainWindow w;
        w.show();
        return a.exec();
    }

    // CONSOLE MODE
    const QStringList args = parser.positionalArguments();

    InkscapeTools tools;
    QStringList svgPathList;

    // load config file, if --setting-file given.
    if( parser.isSet(settingFileOption) )
    {
        if( QFile::exists(parser.value(settingFileOption)) )
            s->loadSettingFile(parser.value(settingFileOption));
    }

    // list colors in file, if option --find-colors give
    if( parser.isSet(findColorsOption) && !args.isEmpty() && args.at(0).endsWith(".svg") )
    {
        QString path = QDir::currentPath() + "/" + args.at(0);
        qDebug() << path;
        if( QFile::exists(path) )
        {
            foreach(const QColor &color, tools.findColors(path))
                qDebug() << color.name() << color.redF() << color.greenF() << color.blueF();
        }
    }
    /// Creation of the job list:
    // case no argument: all svg in current directory
    else if( args.isEmpty() )
    {
        svgPathList = InkscapeTools::svgInDirectory(QDir::currentPath());
    }
    // case argument is a directory name: all svg in this directory
    else if( ! args.at(0).endsWith(".svg") )
    {
        svgPathList = InkscapeTools::svgInDirectory(args.at(0));
    }
    // case argument is a file name: this file only
    else
    {
        svgPathList << QFileInfo(args.at(0)).absoluteFilePath();
    }

    /// do the job
    qDebug() << "Job list:" << svgPathList;
    tools.replaceColors(svgPathList);

    qDebug() << "exit.";
    return 0;
}


