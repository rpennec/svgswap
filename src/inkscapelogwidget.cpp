#include "inkscapelogwidget.h"
#include <QVBoxLayout>
#include <QLabel>

InkscapeLogWidget::InkscapeLogWidget(QWidget *parent)
    : QWidget(parent)
    , m_inkscapeNotFoundLabel( new QLabel(this) )
{
    m_inkscapeNotFoundLabel->setText("<font color=\"red\">Inkscape not found!</font><br/>"
                                   "Please visit <a href=\"https://inkscape.org/en/\">inkscape.org</a>"
                                     " and install it.");

    QVBoxLayout *layout = new QVBoxLayout;
    layout->setSpacing(0);
    layout->addWidget(m_inkscapeNotFoundLabel);

    this->setLayout(layout);
}

void InkscapeLogWidget::setInkscapeFound(bool found)
{
    m_inkscapeNotFoundLabel->setVisible(!found);
}

