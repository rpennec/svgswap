/****************************************************************************
**
**  Copyright (C) 2015-2016 Romain Pennec.
**  Contact: romain.pennec@tum.de
**
**  This file is a part of the svgswap project.
**  https://gitlab.com/rpennec/svgswap
**
**  $QT_BEGIN_LICENSE:GPL$
**
**  svgswap is free software: you can redistribute it and/or
**  modify it under the terms of the GNU General Public License as published
**  by the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  svgswap is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with svgswap.
**  If not, see <http://www.gnu.org/licenses/>.
**
** $QT_END_LICENSE$
**
****************************************************************************/

#ifndef GRAPHICSWIDGET_H
#define GRAPHICSWIDGET_H

#include <QSvgWidget>
#include <QTemporaryFile>

class GraphicsWidget : public QSvgWidget
{
    Q_OBJECT

public:
    explicit GraphicsWidget(QWidget *parent = 0);
    void loadTemp(const QString &svgName);
    QString tempFileName() const;

private:
    QTemporaryFile m_file;

};

#endif // GRAPHICSWIDGET_H
