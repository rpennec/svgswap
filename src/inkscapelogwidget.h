#ifndef INKSCAPELOGWIDGET_H
#define INKSCAPELOGWIDGET_H

#include <QWidget>
QT_FORWARD_DECLARE_CLASS(QLabel)

class InkscapeLogWidget : public QWidget
{
    Q_OBJECT

public:
    explicit InkscapeLogWidget(QWidget *parent = 0);

public slots:
    void setInkscapeFound(bool found);

private:
    QLabel *m_inkscapeNotFoundLabel;
};

#endif // INKSCAPELOGWIDGET_H
