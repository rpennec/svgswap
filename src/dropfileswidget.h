/****************************************************************************
**
**  Copyright (C) 2015-2016 Romain Pennec.
**  Contact: romain.pennec@tum.de
**
**  This file is a part of the svgswap project.
**  https://gitlab.com/rpennec/svgswap
**
**  $QT_BEGIN_LICENSE:GPL$
**
**  svgswap is free software: you can redistribute it and/or
**  modify it under the terms of the GNU General Public License as published
**  by the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  svgswap is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with svgswap.
**  If not, see <http://www.gnu.org/licenses/>.
**
** $QT_END_LICENSE$
**
****************************************************************************/

#ifndef DROPFILESWIDGET_H
#define DROPFILESWIDGET_H

#include <QWidget>
#include <QStringList>

class QFrame;
class QLabel;

class DropFilesWidget : public QWidget
{
    Q_OBJECT

public:
    DropFilesWidget(QWidget *parent=0);
    void setFilter(const QString &filter);
    QString filter() const;
    void setText(const QString &text);
    void setMultipleDropEnabled(bool enabled);

signals:
    void textClicked();
    void fileDropped(const QStringList &paths);

public slots:
    void dialogSelectPaths();

protected:
    void dragEnterEvent(QDragEnterEvent *event);
    void dropEvent(QDropEvent *event);
    bool eventFilter(QObject *obj, QEvent *event);

private:
    QFrame *m_frame;
    QLabel *m_label;
    QString m_filter;
    QStringList m_extensions;
    bool m_multipleDropEnabled;
};

#endif // DROPFILESWIDGET_H
