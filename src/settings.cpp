/****************************************************************************
**
**  Copyright (C) 2015-2016 Romain Pennec.
**  Contact: romain.pennec@tum.de
**
**  This file is a part of the svgswap project.
**  https://gitlab.com/rpennec/svgswap
**
**  $QT_BEGIN_LICENSE:GPL$
**
**  svgswap is free software: you can redistribute it and/or
**  modify it under the terms of the GNU General Public License as published
**  by the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  svgswap is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with svgswap.
**  If not, see <http://www.gnu.org/licenses/>.
**
** $QT_END_LICENSE$
**
****************************************************************************/

#include "settings.h"
#include <QApplication>
#include <QColorDialog>
#include <QSettings>
#include <QDebug>

Settings::Settings(QObject *parent)
    : QObject(parent)
    , m_settings(NULL)
{

}

void Settings::initialize()
{
    qDebug() << tr("Initialize settings");
    sttgs()->sync();
}

bool Settings::runInkscapeAfterReplacement() const
{
    return sttgs()->value("RunInkscapeAfter",false).toBool();
}

bool Settings::rememberChoices() const
{
    return sttgs()->value("RememberChoices",true).toBool();
}

bool Settings::manualReplacement() const
{
    return sttgs()->value("ManualReplacement",true).toBool();
}

bool Settings::createBackups() const
{
    return sttgs()->value("CreateBackups",false).toBool();
}

bool Settings::overwrite() const
{
    return sttgs()->value("Overwrite",true).toBool();
}

ColorPairs Settings::replacementPairs() const
{
    ColorPairs pairs;
    sttgs()->beginGroup("ReplaceColors");
    foreach(const QString &key, sttgs()->childKeys())
    {
        pairs << ColorPair(key, sttgs()->value(key).toString());
    }
    sttgs()->endGroup();
    return pairs;
}

void Settings::loadSettingFile(const QString &filePath)
{
    if( m_settings )
    {
        delete m_settings;
        m_settings = NULL;
    }
    m_settings = new QSettings(filePath,QSettings::IniFormat,this);
}

void Settings::setReplaceColor(const ColorPair &pair)
{
    sttgs()->beginGroup("ReplaceColors");
    QString key = pair.before().name(QColor::HexRgb);
    QString value = pair.after().name(QColor::HexRgb);
    sttgs()->setValue(key,value);
    sttgs()->endGroup();
}

void Settings::loadCustomColors()
{
    sttgs()->sync();
    if( sttgs()->childGroups().contains("CustomColors") )
    {
        sttgs()->beginGroup("CustomColors");
        int c=0;
        foreach(const QString &colorkey, sttgs()->childKeys())
        {
            QColorDialog::setCustomColor(c++,QColor(sttgs()->value(colorkey).toString()));
        }
        sttgs()->endGroup();
    }
}

void Settings::saveCustomColors()
{
    sttgs()->sync();
    sttgs()->beginGroup("CustomColors");
    for(int i=0; i < QColorDialog::customCount(); ++i)
    {
        sttgs()->setValue("Custom"+QString::number(i),QColorDialog::customColor(i).name());
    }
    sttgs()->endGroup();
}

void Settings::setRunInkscapeAfterReplacement(bool run)
{
    sttgs()->setValue("RunInkscapeAfter",run);
}

void Settings::setRememberChoices(bool remember)
{
    sttgs()->setValue("RememberChoices",remember);
}

void Settings::setManualReplacement(bool manual)
{
    sttgs()->setValue("ManualReplacement",manual);
}

void Settings::setCreateBackups(bool backup)
{
    sttgs()->setValue("CreateBackups",backup);
}

void Settings::setOverwrite(bool overwrite)
{
    sttgs()->setValue("Overwrite",overwrite);
}

/**
 * It seems a little overcomplicated. Maybe this is not the best way.
 * The difficulty is that an instance of QApplication must be created
 * before m_settings is initiallized.
 */
QSettings *Settings::sttgs() const
{
    if( ! m_settings )
    {
        Settings *self = const_cast<Settings *>(this);
        self->setQSettings(new QSettings(qApp->organizationName(),qAppName(),qApp));
    }
    return m_settings;
}

void Settings::setQSettings(QSettings *s)
{
    m_settings = s;
    qDebug() << "Settings in" << sttgs()->fileName();
}

Settings *s = new Settings(qApp);

