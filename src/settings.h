/****************************************************************************
**
**  Copyright (C) 2015-2016 Romain Pennec.
**  Contact: romain.pennec@tum.de
**
**  This file is a part of the svgswap project.
**  https://gitlab.com/rpennec/svgswap
**
**  $QT_BEGIN_LICENSE:GPL$
**
**  svgswap is free software: you can redistribute it and/or
**  modify it under the terms of the GNU General Public License as published
**  by the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  svgswap is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with svgswap.
**  If not, see <http://www.gnu.org/licenses/>.
**
** $QT_END_LICENSE$
**
****************************************************************************/

#ifndef SETTINGS_H
#define SETTINGS_H

#include <QObject>
QT_FORWARD_DECLARE_CLASS(QSettings)

#include "colorpairs.h"

class Settings : public QObject
{
public:
    Settings(QObject *parent=0);
    void initialize();
    bool runInkscapeAfterReplacement() const;
    bool rememberChoices() const;
    bool manualReplacement() const;
    bool createBackups() const;
    bool overwrite() const;
    ColorPairs replacementPairs() const;
    void loadSettingFile(const QString &filePath);

public slots:
    void setReplaceColor(const ColorPair &pair);
    void loadCustomColors();
    void saveCustomColors();
    void setRunInkscapeAfterReplacement(bool run);
    void setRememberChoices(bool remember);
    void setManualReplacement(bool manual);
    void setCreateBackups(bool backup);
    void setOverwrite(bool overwrite);

protected:
    QSettings * sttgs() const;

private:
    void setQSettings(QSettings *s);
    QSettings * m_settings;
};

extern Settings *s;

#endif // SETTINGS_H
