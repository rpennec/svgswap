/****************************************************************************
**
**  Copyright (C) 2015-2016 Romain Pennec.
**  Contact: romain.pennec@tum.de
**
**  This file is a part of the svgswap project.
**  https://gitlab.com/rpennec/svgswap
**
**  $QT_BEGIN_LICENSE:GPL$
**
**  svgswap is free software: you can redistribute it and/or
**  modify it under the terms of the GNU General Public License as published
**  by the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  svgswap is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with svgswap.
**  If not, see <http://www.gnu.org/licenses/>.
**
** $QT_END_LICENSE$
**
****************************************************************************/

#include "mainwindow.h"

#include <QApplication>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QGridLayout>
#include <QFileDialog>
#include <QMessageBox>
#include <QPushButton>
#include <QMenu>
#include <QMenuBar>
#include <QStatusBar>
#include <QProgressDialog>
#include <QDialogButtonBox>
#include <QToolBar>
#include <QTime>
#include <QDockWidget>
#include <QDebug>

#include "settings.h"
#include "inkscapetools.h"
#include "dropfileswidget.h"
#include "settingswidget.h"
#include "colorpairswidget.h"
#include "graphicswidget.h"
#include "converterwidget.h"
#include "inkscapelogwidget.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , m_tools( new InkscapeTools(this) )
    , m_stack( new QStackedWidget(this) )
    , m_settingsWidget( new SettingsWidget(this) )
    , m_dropWidget( new DropFilesWidget(this) )
    , m_converterWidget( new ConverterWidget(this) )
    , m_inkscapeDockWidget( new QDockWidget(this) )
{
    // settings
    this->setMinimumSize(400,300);
    this->setMaximumHeight(550);
    s->loadCustomColors();
    m_converterWidget->setConverter(m_tools);
    this->setWindowFlags(this->windowFlags()|Qt::WindowStaysOnTopHint);

    // dock widget for inkscape messages
    InkscapeLogWidget * inkscapeLogWidget = new InkscapeLogWidget(m_inkscapeDockWidget);
    m_inkscapeDockWidget->setFeatures(QDockWidget::NoDockWidgetFeatures);
    m_inkscapeDockWidget->setWidget(inkscapeLogWidget);
    this->addDockWidget(Qt::TopDockWidgetArea,m_inkscapeDockWidget);
    connect(m_tools,&InkscapeTools::inkscapeFound,
            inkscapeLogWidget,&InkscapeLogWidget::setInkscapeFound);
    connect(m_tools,&InkscapeTools::inkscapeFound,[=](bool found){
       m_inkscapeDockWidget->setVisible(!found);
    });
    m_tools->checkSoftwares();

    // allowed extensions
    m_dropWidget->setText(tr("Drop SVG files here"));
    m_dropWidget->setFilter("Vector Graphics (*.svg)");

    // assemble central widget
    m_stack->addWidget(m_dropWidget);
    m_stack->addWidget(m_settingsWidget);
    m_stack->addWidget(m_converterWidget);
    this->setCentralWidget(m_stack);

    // create actions
    QAction *actShowSettings = new QAction(QIcon::fromTheme("preferences-desktop",QIcon(":/icon/preferences-desktop")),"Settings",this);
    QAction *actShowConverter = new QAction(QIcon(":/icon/applications-utilities"),"Converter",this);
    QAction *actQuit = new QAction(QIcon::fromTheme("application-exit",QIcon(":/icon/application-exit")),"Quit",this);
    QAction *actHelp = new QAction(QIcon::fromTheme("help-about",QIcon(":/icon/help-about")),"About",this);
    QAction *actAboutQt = new QAction(QIcon(":/logo/Qt.png"),"Qt",this);
    connect(actQuit, &QAction::triggered, this, &MainWindow::close);
    connect(actShowSettings, &QAction::triggered, this, &MainWindow::showSettings);
    connect(actShowConverter, &QAction::triggered, this, &MainWindow::showConverter);
    connect(actHelp, &QAction::triggered, this, &MainWindow::about);
    connect(actAboutQt, &QAction::triggered, qApp, &QApplication::aboutQt);

    // create toolbar
    QToolBar *toolBar = new QToolBar(this);
    toolBar->setMovable(false);
    toolBar->setToolButtonStyle(Qt::ToolButtonTextBesideIcon);
    toolBar->addAction(actShowSettings);
    toolBar->addAction(actShowConverter);
    toolBar->addAction(actHelp);
    toolBar->addAction(actAboutQt);
    toolBar->addAction(actQuit);
    this->addToolBar(Qt::BottomToolBarArea,toolBar);

    // connections
    connect(m_settingsWidget, &SettingsWidget::okClicked, this, &MainWindow::showDropArea);
    connect(m_dropWidget, &DropFilesWidget::fileDropped, this, &MainWindow::onDroppedFiles);
    connect(m_converterWidget, &ConverterWidget::done, this, &MainWindow::showDropArea);

    // timer (trigger timerEvent)
    m_timer.start(2000,Qt::VeryCoarseTimer,this);
}

MainWindow::~MainWindow()
{
    s->saveCustomColors();
}

///@todo improve about page
void MainWindow::about()
{
    QMessageBox::about(this, tr("About Application"),
        tr("<h3>SvgSwap version %1</h3>"
           "<p>Author: Romain Pennec</p>"
           "<p>License: GNU GPL v3</p>"
           "<p>Project hosted on <a href=%2>gitlab</a></p>"
           ).arg(qApp->applicationVersion())
            .arg("https://gitlab.com/rpennec/svgswap"));
}


void MainWindow::showSettings()
{
    m_stack->setCurrentWidget(m_settingsWidget);
    this->findChild<QToolBar*>()->hide();
}

void MainWindow::showDropArea()
{
    m_stack->setCurrentWidget(m_dropWidget);
    this->findChild<QToolBar*>()->show();
}

void MainWindow::showConverter()
{
    m_stack->setCurrentWidget(m_converterWidget);
    this->findChild<QToolBar*>()->hide();
}

/// reaction to signal droppedFiles
void MainWindow::onDroppedFiles(const QStringList &list)
{
    int nbFiles = list.count();
    this->statusBar()->showMessage(tr("INFO: received %1 valid svg files").arg(nbFiles),5000);

    if( s->manualReplacement() && nbFiles > 1 )
    {
        QMessageBox::warning(this,tr("Too many files"),"In manual mode, you can only drop one file.\n"
                             "Using first file.");
    }

    if( list.isEmpty() )
        return ;

    // manual color replacement (by the user)
    if( s->manualReplacement() )
    {
        this->manualColorReplacment(list.first());
    }
    // automatic color replacement
    else
    {
        // Progression dialog
        QProgressDialog dialog("Replacing colors...",tr("Cancel"),0,nbFiles,this);
        dialog.setWindowModality(Qt::WindowModal);
        dialog.setValue(0);

        for(int i=0; i < nbFiles ; i++)
        {
            dialog.setValue(i);
            if( dialog.wasCanceled() )
                break;

            m_tools->replaceColors(list.at(i));
        }
        dialog.setValue(nbFiles);
        this->statusBar()->showMessage(tr("INFO: color replacement finished"),5000);
    }

}

void MainWindow::manualColorReplacment(const QString &path)
{
    // what colors are in the given svg file?
    QList<QColor> listColors = m_tools->findColors(path);

    // create dialog widgets ( colors-pairs + dialog-buttons + preview )
    QDialog *dialog = new QDialog(this);
    QVBoxLayout *vlayout = new QVBoxLayout;
    ColorPairsWidget *widget = new ColorPairsWidget(listColors,dialog);
    QDialogButtonBox *buttons = new QDialogButtonBox(QDialogButtonBox::Cancel|QDialogButtonBox::Ok,Qt::Horizontal,dialog);
    GraphicsWidget *view = new GraphicsWidget(dialog);
    view->setMaximumSize(300,300);

    // load image (as temporary file)
    view->loadTemp(path);

    // assemble dialog
    QHBoxLayout *hlayout = new QHBoxLayout;
    hlayout->addWidget(widget);
    hlayout->addWidget(view);
    vlayout->addLayout(hlayout);
    vlayout->addWidget(buttons);
    dialog->setLayout(vlayout);

    // connections
    connect(widget, &ColorPairsWidget::pairChanged, [=](const ColorPair &pair){
        m_tools->replaceColors(view->tempFileName(),ColorPairs()<<pair);
        view->load(view->tempFileName());
    });
    connect(buttons,&QDialogButtonBox::accepted,dialog,&QDialog::accept);
    connect(buttons,&QDialogButtonBox::rejected,dialog,&QDialog::reject);

    // do the changes and update history
    if ( dialog->exec() == QDialog::Accepted )
    {
        m_tools->replaceColors(path,widget->pairs());
        widget->addToHistory(widget->pairs());
    }
}

void MainWindow::timerEvent(QTimerEvent *event)
{
    if( event->timerId() == m_timer.timerId() )
        statusBar()->setVisible(!statusBar()->currentMessage().isEmpty());
    else
        QMainWindow::timerEvent(event);
}




