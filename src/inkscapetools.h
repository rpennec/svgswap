/****************************************************************************
**
**  Copyright (C) 2015-2016 Romain Pennec.
**  Contact: romain.pennec@tum.de
**
**  This file is a part of the svgswap project.
**  https://gitlab.com/rpennec/svgswap
**
**  $QT_BEGIN_LICENSE:GPL$
**
**  svgswap is free software: you can redistribute it and/or
**  modify it under the terms of the GNU General Public License as published
**  by the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  svgswap is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with svgswap.
**  If not, see <http://www.gnu.org/licenses/>.
**
** $QT_END_LICENSE$
**
****************************************************************************/

#ifndef INKSCAPETOOLS_H
#define INKSCAPETOOLS_H

#include <QObject>
#include <QStringList>
#include "colorpairs.h"

QT_FORWARD_DECLARE_CLASS(QProcess)

class InkscapeTools : public QObject
{
    Q_OBJECT

public:
    enum Export { Latex, Pdf, Svg, Png, Eps };

public:
    InkscapeTools(QObject *parent=0);
    static QStringList svgInDirectory(const QString &dirPath);
    static QList<QColor> findColors(const QString &filePath, bool rcf=true);
    static QColor colorFromSVGrgb(const QString &rgbStr);
    QString texPreamble() const;
    void setTexPreamble(const QString &texPreamble);

signals:
    void inskapeFinished(bool success);
    void pdflatexFinished(bool success);
    void replacedColors(const QString &fileName);
    void inkscapeFound(bool found);
    void pdflatexFound(bool found);

public slots:
    void replaceColors(const QString &filePath);
    void replaceColors(const QString &filePath, const ColorPairs &pairs);
    void replaceColors(const QStringList &filePaths);
    void runInkscape(const QString &filePath, Export mode=Latex);
    void runPdfLatex(const QString &filePath);
    bool generateTexFileFor(const QString &pdf_texFile);
    void checkSoftwares();

private:
    QProcess *m_inkscape;
    QProcess *m_pdflatex;
    QString m_errorString;
    bool m_inkscapeInstalled;
    bool m_epstopdfInstalled;
    bool m_pdftosvgInstalled;
    bool m_pdflatexInstalled;
    QString m_texPreamble;
};


#endif // INKSCAPETOOLS_H
