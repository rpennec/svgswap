/****************************************************************************
**
**  Copyright (C) 2015-2016 Romain Pennec.
**  Contact: romain.pennec@tum.de
**
**  This file is a part of the svgswap project.
**  https://gitlab.com/rpennec/svgswap
**
**  $QT_BEGIN_LICENSE:GPL$
**
**  svgswap is free software: you can redistribute it and/or
**  modify it under the terms of the GNU General Public License as published
**  by the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  svgswap is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with svgswap.
**  If not, see <http://www.gnu.org/licenses/>.
**
** $QT_END_LICENSE$
**
****************************************************************************/

#include "colorpairswidget.h"
#include <QLabel>
#include <QDebug>
#include <QtMath>
#include <QVBoxLayout>

#include "settings.h"

ColorPairs ColorPairsWidget::s_history;

ColorPairsWidget::ColorPairsWidget(const ColorPairs &pairs, QWidget *parent)
    : QWidget(parent)
    , m_pairs(pairs)
    , m_colorGrid(new QGridLayout)
{
    QVBoxLayout *vlayout = new QVBoxLayout;

    if( pairs.count() > MAX_NB_COLOR_IN_SVG )
    {
        vlayout->addWidget(new QLabel(tr("I cannot display %1 colors! \n"
                                         "Maybe in a future release").arg(pairs.count()),this));
    }
    else
    {
        // Assemble grid layout:
        //   (l,0)      (l,1)    (l ,2)  (l,3)     (l,4)
        //   Replace    <Color1> by      <Color2>  ""
        //
        int l=0;
        foreach(const ColorPair &pair, m_pairs)
        {
            ColorButton *bLeft = new ColorButton(pair.first,this);
            ColorButton *bRight = new ColorButton(pair.second,this);

            m_colorGrid->addWidget(new QLabel("Replace",this),l,0);
            m_colorGrid->setAlignment(m_colorGrid->itemAtPosition(l,0)->widget(),Qt::AlignCenter);
            m_colorGrid->addWidget(bLeft,l,1);
            m_colorGrid->addWidget(new QLabel("By",this),l,2);
            m_colorGrid->setAlignment(m_colorGrid->itemAtPosition(l,2)->widget(),Qt::AlignCenter);
            m_colorGrid->addWidget(bRight,l,3);
            m_colorGrid->addWidget(new QLabel("",this),l++,4);

            connect(bLeft, &ColorButton::colorChanged, this, &ColorPairsWidget::updatePairs);
            connect(bRight,&ColorButton::colorChanged, this, &ColorPairsWidget::updatePairs);
            m_colorButtons << bLeft << bRight;
        }
        vlayout->addLayout(m_colorGrid);
    }
    this->setLayout(vlayout);
    this->setMaximumHeight(800);
}

ColorPairsWidget::ColorPairsWidget(const QList<QColor> &list, QWidget *parent)
    : ColorPairsWidget(initializePairs(list),parent)
{

}

ColorPairs ColorPairsWidget::pairs() const
{
    return m_pairs;
}

void ColorPairsWidget::addToHistory(const ColorPairs &pairs)
{
    s_history << pairs;
}

ColorPairs ColorPairsWidget::history() const
{
    return s_history;
}

QColor ColorPairsWidget::replacementColorFor(const QColor &before) const
{
    QColor replace(before.name());

    if( s->rememberChoices() )
    {
        // check in history if a choice has already been made for this color
        foreach(const ColorPair &pair, s_history)
        {
            if( before == pair.first )
                replace = pair.second;
        }
    }
    return replace;
}

void ColorPairsWidget::updatePairs()
{
    ColorButton *button = qobject_cast<ColorButton*>(this->sender());

    if( button )
    {
        int index =m_colorGrid->indexOf(button);
        int q = qFloor(qreal(index)/5.0);
        int r = index - (5*q);
        if ( r == 1)
            m_pairs[q].first = button->color().name();
        else
            m_pairs[q].second = button->color().name();

        emit pairChanged(m_pairs.at(q));
    }
}

void ColorPairsWidget::addLineInColorGrid()
{
    int l = m_colorButtons.count();
    ColorPair pair(QColor(Qt::black),QColor(Qt::black));
    ColorButton *bLeft = new ColorButton(pair.first,this);
    ColorButton *bRight = new ColorButton(pair.second,this);
    m_colorGrid->addWidget(new QLabel("Replace",this),l,0);
    m_colorGrid->setAlignment(m_colorGrid->itemAtPosition(l,0)->widget(),Qt::AlignCenter);
    m_colorGrid->addWidget(bLeft,l,1);
    m_colorGrid->addWidget(new QLabel("By",this),l,2);
    m_colorGrid->setAlignment(m_colorGrid->itemAtPosition(l,2)->widget(),Qt::AlignCenter);
    m_colorGrid->addWidget(bRight,l,3);
    m_colorGrid->addWidget(new QLabel("",this),l++,4);

    connect(bLeft, &ColorButton::colorChanged, this, &ColorPairsWidget::updatePairs);
    connect(bRight,&ColorButton::colorChanged, this, &ColorPairsWidget::updatePairs);
    m_colorButtons << bLeft << bRight;
    m_pairs << pair;
}

/**
 * @brief ColorPairsWidget::initializePairs
 * @param list colors on the left/before side
 * @return list color pairs (both left/before and right/after side)
 *
 * Color on the right/after side is the same as the one on the left/before side
 * except if history indicates otherwise
 */
ColorPairs ColorPairsWidget::initializePairs(const QList<QColor> &list) const
{
    ColorPairs pairs;

    foreach(const QColor &color, list)
        pairs << ColorPair(color.name(),replacementColorFor(color).name());

    return pairs;
}
